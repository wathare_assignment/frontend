import './App.css';
import Chart from './components/Chart';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Live Data Updates</h1>
        <Chart />
      </header>
    </div>
  );
}

export default App;
