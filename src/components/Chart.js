import React, { useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';

const Chart = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    // Fetch data from the backend API
    fetch('/api/data')
      .then((response) => response.json())
      .then((data) => setData(data));
  }, []);

  // Transform data into chart-friendly format
  const chartData = {
    labels: data.map((item) => new Date(item.timestamp).toLocaleTimeString()),
    datasets: [
      {
        label: 'Live Data Updates',
        data: data.map((item) => item.value),
        fill: false,
        borderColor: 'rgba(75,192,192,1)',
        borderWidth: 2,
      },
    ],
  };

  return <Line data={chartData} />;
};

export default Chart;
